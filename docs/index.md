# Documentation de l'application économat

![logoEconomat](img/logoEconomat.png){width=100%}

L'application n'est pas sur la DMZ. De l'extérieur, utilisez un VPN.

L'intégralité de l'application n'est accessible qu'aux personnes connectées et autorisées.

L'application est développée avec PHP et le framework Laravel dans sa version 10.

Lien vers [l'application](https://economat.2isa.fr/)
