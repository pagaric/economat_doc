# Authentification

L'authentification se fait à l'aide d'**OpenID** et **Azure AD**

## Mise en place de l'authentification

L'authentification est confiée au package [**socialite**](https://laravel.com/docs/10.x/socialite) de Laravel.
Ainsi qu'au package spécifique à [**Micosoft Azure**](https://laravel.com/docs/10.x/socialite).

### installation

```bash
composer require laravel/socialite
composer require socialiteproviders/microsoft-azure
```

### configuration

Dans le fichier `.env` il faut noter les variables suivantes. Ces données sont gérées par le SI.

```
...
AZURE_CLIENT_ID=0c-xxxxxxxxxxxxxxxxxxxxxxxxxxx
AZURE_CLIENT_SECRET=SV-xxxxxxxxxxxxxxxxxxxxxx
AZURE_TENANT_ID=eb-xxxxxxxxxxxxxxxxxxxxxxxxxxxx
AZURE_REDIRECT_URI=https://economat.2isa.fr/auth/callback
...
```
Ensuite, ajouter dans le fichier `config/service.php` le code suivant :

```perl
...

'azure' => [    
        'client_id' => env('AZURE_CLIENT_ID'),
        'client_secret' => env('AZURE_CLIENT_SECRET'),
        'redirect' => env('AZURE_REDIRECT_URI'),
        'tenant' => env('AZURE_TENANT_ID'),
        'proxy' => env('PROXY')  // optionally
      ],
```

On ajoute un *event listener* dans le fichier `app/Providers/EventServiceProvider.php` :

```perl title="app/Providers/EventServiceProvider.php"
protected $listen = [
    
    // ...

    \SocialiteProviders\Manager\SocialiteWasCalled::class => [
        // ... other providers
        \SocialiteProviders\Azure\AzureExtendSocialite::class.'@handle',
    ],
];
```
